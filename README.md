# Synopsis on Rowhammer

This paper is a synopsis of the Rowhammer attack in general, and with 
the specific focus point on the ECCploit variant of Rowhammer.

ECCploit is a Rowhammer attack against ECC memory.
